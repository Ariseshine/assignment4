class Post < ApplicationRecord
	validates :title, presence: true
	validates :title, length: {maximum: 500}
	belongs_to :category
end
