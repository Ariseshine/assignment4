class Category < ApplicationRecord
	validates :name, presence: true
	validates :name, length: {maximum: 500}
	has_many :posts
end
